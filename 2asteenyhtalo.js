const RatkaiseNappi     = document.getElementById("RatkaiseNappi");
const RatkaisuX1        = document.getElementById("RatkaisuX1");
const RatkaisuX2        = document.getElementById("RatkaisuX2");
const RatkaisuTeksti    = document.getElementById("RatkaisuTeksti");

let arvoA = document.getElementById("A");
let arvoB = document.getElementById("B");
let arvoC = document.getElementById("C");

RatkaiseNappi.addEventListener("click", RatkaiseYhtalo);

function RatkaiseYhtalo()
{
    let a = parseFloat(arvoA.value);
    let b = parseFloat(arvoB.value);
    let c = parseFloat(arvoC.value);

    let D = b*b-4*a*c;
        
    if  (D > 0) 
    RatkaisuTeksti.innerHTML = "Yhtälöllä on kaksi erisuurta ratkaisua X1 ja X2.";

    if  (D === 0)
    RatkaisuTeksti.innerHTML = "Yhtälöllä on kaksinkertainen ratkaisu eli kaksoisjuuri X1, X2";

    if (D < 0)
    RatkaisuTeksti.innerHTML = "Yhtälöllä ei ole reaalisia ratkaisuja.";

    let x1 =-b/2/a+Math.pow(Math.pow(b,2)-4*a*c,0.5)/2/a;
    let x2 =-b/2/a-Math.pow(Math.pow(b,2)-4*a*c,0.5)/2/a;

    RatkaisuX1.value = x1;
    RatkaisuX2.value = x2;
}
